
export const alsoS = <ObjType>(obj: ObjType, block: (obj: ObjType) => void) => {
	block(obj)
	return obj
}

export const letS = <ObjType, ResultType>(obj: ObjType, block: (obj: ObjType) => ResultType) => {
	return block(obj)
}
