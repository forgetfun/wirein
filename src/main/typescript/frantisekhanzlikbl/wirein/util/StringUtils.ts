
export const countOccurences = (source: string, part: string) => {
	return (source.match(part) || []).length
}
