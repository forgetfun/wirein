
import Fs from "fs";
import Os from "os";
import Path from "path";

import fancyLog from "fancy-log";

import HoconMetadataReader from "./metadata/HoconMetadataReader";
// import MetadataReader from "./metadata/MetadataReader";
import FsUtils from "./util/FsUtils";
import { InclusionActionType, filterFilesByMetadata } from "./util/MetadataUtils";
import { forEach, find } from "./util/PromiseUtils";
import { alsoS } from "./util/ScopeFunctions";

export default class Wirein {
	protected metadataReader = new HoconMetadataReader()
	protected defaultLocation = `${Os.homedir()}/user_data`
	protected defaultDirectoryInclusionActionType = InclusionActionType.INCLUDE
	public main(args: string[]) {
		this.wirein(this.defaultLocation)
	}
	public async wirein(path: string) {
		fancyLog(`Checking "${path}"`)
		if ((await Fs.promises.stat(path)).isDirectory()) {
			const subfiles = (await Fs.promises.readdir(path)).map(subfile => Path.resolve(path, subfile))
			const directoryMetadataFilePath = await find(subfiles, async subfile => !(await Fs.promises.stat(subfile)).isDirectory() && this.metadataReader.isDirectoryMetadataFile(subfile))
			if (directoryMetadataFilePath == undefined) {
				// fancyLog(`Directory metadata not found in "${path}"`)
				if (this.defaultDirectoryInclusionActionType == InclusionActionType.INCLUDE)
					await forEach(subfiles, async subfile => {
						await this.wirein(subfile)
					})
			} else await forEach(
				filterFilesByMetadata(subfiles, alsoS(await this.metadataReader.getMetadataForDirectoryMetadataFile(directoryMetadataFilePath), metadata => {
					fancyLog(`Found metadata for "${path}", includes: [${metadata.includes}], excludes: [${metadata.excludes}]`)
				}), this.defaultDirectoryInclusionActionType),
				async subfile => {
					await this.wirein(subfile)
				}
			)
		} else {
			if (!this.metadataReader.isFileMetadataFile(path)) return
			fancyLog(`Found FileMetadataFile: "${path}"`)
			const metadata = await this.metadataReader.getMetadataForFileMetadataFile(path)
			const destination = Path.resolve(`${Os.homedir()}`, metadata.destination)
			if (await FsUtils.pathExists(destination, true)) fancyLog.warn(`Destination "${destination}" already exists, skipping.`)
			else {
				const destinationParent = Path.dirname(destination)
				await Fs.promises.mkdir(destinationParent, { recursive: true })
				await this.createRelativeSymlink(destination, metadata.target)
			}
		}
	}
	/**
	 * Creates new link at given `destination` pointing at given `target` by relative path
	 * @param destination destination of the new link, this includes the link's name
	 * @param target new symlink's target
	 */
	protected async createRelativeSymlink(destination: string, target: string) {
		const destinationParent = Path.dirname(destination)
		const relativeTarget = Path.relative(destinationParent, target)
		fancyLog.info(`Creating symlink at "${destination}" pointing at "${relativeTarget}"`)
		await Fs.promises.symlink(relativeTarget, destination)
	}
}

(new Wirein()).main(process.argv)
