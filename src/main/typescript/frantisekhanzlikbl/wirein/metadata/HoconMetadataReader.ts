
import Fs from "fs"
import Path from "path"

import hoconParser from "hocon-parser"

import MetadataReader from "./MetadataReader"
import FileMetadata from "./FileMetadata"
import DirectoryMetadata from "./DirectoryMetadata"
import { alsoS } from "../util/ScopeFunctions";

export interface HoconMetadataObject {
	destination: string
}

export default class HoconMetadataReader implements MetadataReader {
	static readonly fileMetadataFileNamePattern = /.\.wirein_metadata\.hocon$/
	static readonly directoryMetadataFileNamePattern = /^\.wirein_metadata\.hocon$/
	public isFileMetadataFile(metadataFilePath: string) {
		return HoconMetadataReader.fileMetadataFileNamePattern.test(Path.basename(metadataFilePath))
	}
	public isDirectoryMetadataFile(metadataFilePath: string) {
		return HoconMetadataReader.directoryMetadataFileNamePattern.test(Path.basename(metadataFilePath))
	}
	public getTargetNameForMetadataFile(metadataFilePath: string) {
		return metadataFilePath.slice(0, metadataFilePath.search(HoconMetadataReader.fileMetadataFileNamePattern) + 1)
	}
	public async getMetadataForFileMetadataFile(metadataFilePath: string) {
		return alsoS(hoconParser((await Fs.promises.readFile(metadataFilePath)).toString()) as FileMetadata, fileMetadata => {
			if (fileMetadata.target == undefined) fileMetadata.target = this.getTargetNameForMetadataFile(metadataFilePath)
		})
	}
	async getMetadataForDirectoryMetadataFile(metadataFilePath: string) {
		return alsoS(hoconParser((await Fs.promises.readFile(metadataFilePath)).toString()) as DirectoryMetadata, directoryMetadata => {
			if (directoryMetadata.includes == undefined) directoryMetadata.includes = []
			if (directoryMetadata.excludes == undefined) directoryMetadata.excludes = []
		})
	}
}
